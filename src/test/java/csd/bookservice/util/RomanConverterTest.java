package csd.bookservice.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class RomanConverterTest {
    private RomanConverter converter = new RomanConverter();
    @Test
    void convertI(){
        assertEquals(1, converter.convertRomanToArabicNumber("I"), "Roman I should equal to 1");
    }
}